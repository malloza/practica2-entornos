package ejercicios;

import java.util.Scanner;

public class Ej4 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		int numeroLeido;
		int resultadoDivision;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		//la Solucion del error es quitar el = en la segunda condicion del FOR 
		for(int i = numeroLeido; i >= 0 ; i--){
			/*El problema es que en el for la i (que es el divisor) llega al valor 0 * 
			y no se puede hacer la divisi�n por lo que salta el problema*/
			resultadoDivision = numeroLeido / i;
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}
		
		
		lector.close();
	}

}
