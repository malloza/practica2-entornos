﻿package ejercicios;

import java.util.Scanner;

public class Ej5 {

	public static void main(String[] args) {
		/*
		 * Ayudate del debugger para entender qué realiza este programa
		 */
		
		//declaramos el Scanner para poder leer de teclado y inicializamos las variables
		Scanner lector;
		int numeroLeido;
		int cantidadDivisores = 0;
		//creamos una variable Scanner y pedimos un valor al usuario
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		
		//Con este bucle for y el if calculamos la cantidad de divisores que tiene y los cuenta 
		for (int i = 1; i <= numeroLeido; i++) {
			if(numeroLeido % i == 0){
				cantidadDivisores++;
			}
		}
		
		//si el numero introducido tiene mas de 2 divisores es primo o no y el programa lo imprime por pantalla 
		if(cantidadDivisores > 2){
			System.out.println("No lo es");
		}else{
			System.out.println("Si lo es");
		}
		
		//creamos el metodo Scanner
		lector.close();
	}

}
