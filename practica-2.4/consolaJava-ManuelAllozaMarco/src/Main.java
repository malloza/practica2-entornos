import java.util.Scanner;
public class Main {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int opcion = 0;boolean quiereSegir=true;String respuesta = "si";
		System.out.println("INTRODUCE UN NUMERO CON EL QUE OPERAR");
		int numero = input.nextInt(); 
		do{
			System.out.println("Elige un de las siguientes opciones");
			System.out.println("         MENU              ");
			System.out.println("1.Ver si el numero es primo");
			System.out.println("2.Sacar los divisores");
			System.out.println("3.Ver si el numero es par");
			System.out.println("4.Ver si el numero es mayor que 5");
			opcion = input.nextInt();
			input.nextLine();
			switch (opcion) {
			case 1:
				verPrimo(numero);
				break;
			case 2:
				calcularDivisores(numero);
				break;
			case 3:
				comprobarPar(numero);
				break;
			case 4:
				mayorCinco(numero);
				break;
			}
			System.out.println("Quieres hacer mas opraciones con el numero");
			respuesta = input.nextLine();
			if(respuesta.equalsIgnoreCase("NO")){
				quiereSegir=false; 
			}
		}while(quiereSegir);
		
		input.close();
	}
	
	static void comprobarPar(int n ){
		if(n%2==0){
			System.out.println(n+" Es un numero par");
		}else{
			System.out.println(n+" No es un numero par");
		}
	}
	
	static void calcularDivisores(int n){
		System.out.println("Los divisores de "+n+" son :");
		for(int i=1 ; i<=n ; i++ ){
			if(n%i==0){
				System.out.print(" "+i);
			}
		}
		System.out.println();
	}
	
	static void verPrimo(int n){
		int numPrimos=0;
		for(int i = 1 ; i <=n ; i++){
			if(n%i==0){
				numPrimos++;
			}
		}
		if(numPrimos<3){
			System.out.println("El numero es primo");
		}else{
			System.out.println("EL numero no es primo");
		}
	}
	
	static void mayorCinco(int n){
		if(n<5){
			System.out.println("El numero "+n+" no es mayor que 5 ");
		}else{
			System.out.println("El numero "+n+" es mayor que 5 ");
		}
	}

}
