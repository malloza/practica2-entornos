﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolaVS_ManuelAllozaMarco
{
    class Program
    {
        static void Main(string[] args)
        {
           
            int opcion = 0; Boolean quiereSegir = true; String respuesta = "si";
            System.Console.WriteLine("INTRODUCE UN NUMERO CON EL QUE OPERAR");
            int numero = System.Console.Read();
            do
            {
                System.Console.WriteLine("Elige un de las siguientes opciones");
                System.Console.WriteLine("         MENU              ");
                System.Console.WriteLine("1.Ver si el numero es primo");
                System.Console.WriteLine("2.Sacar los divisores");
                System.Console.WriteLine("3.Ver si el numero es par");
                System.Console.WriteLine("4.Ver si el numero es mayor que 5");
                opcion = int.Parse(System.Console.ReadLine());
                switch (opcion)
                {
                    case 1:
                        verPrimo(numero);
                        break;
                    case 2:
                        calcularDivisores(numero);
                        break;
                    case 3:
                        comprobarPar(numero);
                        break;
                    case 4:
                        mayorCinco(numero);
                        break;
                }
                System.Console.WriteLine("Quieres hacer mas opraciones con el numero");
                respuesta = System.Console.ReadLine();
                if (respuesta.Equals("NO"))
                {
                    quiereSegir = false;
                }
            } while (quiereSegir);

        }

        static void comprobarPar(int n)
        {
            if (n % 2 == 0)
            {
                System.Console.WriteLine(n + " Es un numero par");
            }
            else
            {
                System.Console.WriteLine(n + " No es un numero par");
            }
        }

        static void calcularDivisores(int n)
        {
            System.Console.WriteLine("Los divisores de " + n + " son :");
            for (int i = 1; i <= n; i++)
            {
                if (n % i == 0)
                {
                    System.Console.Write(" " + i);
                }
            }
            System.Console.WriteLine();
        }

        static void verPrimo(int n)
        {
            int numPrimos = 0;
            for (int i = 1; i <= n; i++)
            {
                if (n % i == 0)
                {
                    numPrimos++;
                }
            }
            if (numPrimos < 3)
            {
                System.Console.WriteLine("El numero es primo");
            }
            else
            {
                System.Console.WriteLine("EL numero no es primo");
            }
        }

        static void mayorCinco(int n)
        {
            if (n < 5)
            {
                System.Console.WriteLine("El numero " + n + " no es mayor que 5 ");
            }
            else
            {
                System.Console.WriteLine("El numero " + n + " es mayor que 5 ");
            }
        }
    }
    }
}
