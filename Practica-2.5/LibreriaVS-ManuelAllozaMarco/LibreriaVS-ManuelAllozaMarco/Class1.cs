﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVS_ManuelAllozaMarco
{
    public class Class1
    {
        public static int suma(int n1, int n2)
        {
            return n1 + n2;
        }
        public static int multiplicacion(int n1, int n2)
        {
            return n1 * n2;
        }
        public static int resta(int n1, int n2)
        {
            if (n2 > n1)
            {
                return n2 - n1;
            }
            return n2 - n1;
        }
        public static int potencia(int b, int exponente)
        {
            int potencia = 1;
            for (int i = 0; i < exponente; i++)
            {
                potencia = potencia * b;
            }
            return potencia;
        }
        public static int facorial(int n)
        {
            int factorial = 1;
            for (int i = 1; i <= n; i++)
            {
                factorial = factorial * i;
            }
            return factorial;
        }
    }
}
