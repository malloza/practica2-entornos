﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVS_ManuelAllozaMarco
{
    public class Class2
    {
        public static void divisores(int n)
        {
            for (int i = 1; i <= n; i++)
            {
                if (n % i == 0)
                {
                    System.Console.WriteLine(i + " es divisor");
                }
            }
        }
        public static void primo(int n)
        {
            int divisores = 0;
            for (int i = 1; i <= n; i++)
            {
                if (n % i == 0)
                {
                    divisores++;
                }
            }
            if (divisores == 3)
            {
               System.Console.WriteLine("No es primo");
            }
            else
            {
                System.Console.WriteLine("Es primo");
            }
        }
        public static void esPar(int n)
        {
            if (n % 2 == 0)
            {
                System.Console.WriteLine("Es par");
            }
            else
            {
                System.Console.WriteLine("No es par");
            }
        }
        public static void positivo(int n)
        {
            if (n >= 0)
            {
                System.Console.WriteLine("Es positivo");
            }
            else if (n == 0)
            {
                System.Console.WriteLine("Es 0 ");
            }
            else
            {
                System.Console.WriteLine("Es negativo");
            }
        }
        public static void mayor(int n1, int n2)
        {
            if (n1 > n2)
            {
                System.Console.WriteLine(n1 + " es mayor que " + n2);
            }
            else
            {
                System.Console.WriteLine(n2 + " es mayor que " + n1);
            }
        }
    }
}
