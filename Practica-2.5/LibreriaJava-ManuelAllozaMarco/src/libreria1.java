public class libreria1 {
	static public int suma(int n1,int n2){
		return n1+n2;
	}
	static public int multiplicacion(int n1,int n2){
		return n1*n2;
	}
	static public int resta(int n1,int n2){
		if(n2>n1){
			return n2 - n1 ;
		}
		return n2-n1;
	}
	static public int potencia(int base,int exponente){
		int potencia = 1 ;
		for(int i = 0 ; i < exponente ;i++){
			potencia = potencia * base;
		}
		return potencia;
	}
	static public int facorial(int n){
		int factorial = 1 ;
		for(int i = 1 ; i <=n ;i++){
			factorial = factorial * i;
		}
		return factorial;
	}
}
