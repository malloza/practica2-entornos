package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.BoxLayout;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JCheckBox;
import java.awt.Color;
import java.awt.Toolkit;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JProgressBar;
import javax.swing.JSpinner;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JLabel;
import javax.swing.JToggleButton;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.SwingConstants;
import javax.swing.JScrollBar;
import javax.swing.JRadioButton;
import javax.swing.JTree;
import javax.swing.JEditorPane;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JToolBar;

public class ManuelAllozaMarco extends JFrame {

	/**
	 * Launch the application.
	 *  @author DAM Manuel Alloza Marco 
	 * @since 14/12/2017
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ManuelAllozaMarco frame = new ManuelAllozaMarco();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ManuelAllozaMarco() {
		getContentPane().setBackground(Color.ORANGE);
		setIconImage(Toolkit.getDefaultToolkit().getImage("F:\\Programacion\\Grupo_4_MetodosyPiramides\\ventana\\src\\gui\\icono.jpg"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(Color.ORANGE);
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem mntmAbrir = new JMenuItem("Abrir");
		mnArchivo.add(mntmAbrir);
		
		JMenuItem mntmNuevo = new JMenuItem("Nuevo");
		mnArchivo.add(mntmNuevo);
		
		JMenuItem mntmGuardar = new JMenuItem("Guardar");
		mnArchivo.add(mntmGuardar);
		
		JMenuItem mntmGuardarComo = new JMenuItem("Guardar como..");
		mnArchivo.add(mntmGuardarComo);
		
		JMenu mnEdicion = new JMenu("Edicion");
		menuBar.add(mnEdicion);
		
		JMenuItem mntmCortar = new JMenuItem("cortar");
		mnEdicion.add(mntmCortar);
		
		JMenuItem mntmEditar = new JMenuItem("editar");
		mnEdicion.add(mntmEditar);
		
		JMenu mnOpciones = new JMenu("opciones");
		mnEdicion.add(mnOpciones);
		
		JMenuItem mntmTamao = new JMenuItem("Tama\u00F1o");
		mnOpciones.add(mntmTamao);
		
		JMenuItem mntmFondoDePantalla = new JMenuItem("Fondo de pantalla ");
		mnOpciones.add(mntmFondoDePantalla);
		
		JMenuItem mntmInsertar = new JMenuItem("insertar");
		mnOpciones.add(mntmInsertar);
		
		JMenu menu = new JMenu("");
		menuBar.add(menu);
		getContentPane().setLayout(null);
		
		JSpinner spinner = new JSpinner();
		spinner.setBackground(Color.ORANGE);
		spinner.setBounds(358, 31, 49, 20);
		getContentPane().add(spinner);
		
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.setBackground(Color.GREEN);
		btnEnviar.setBounds(33, 211, 84, 20);
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		getContentPane().add(btnEnviar);
		
		JSlider slider = new JSlider();
		slider.setBackground(Color.ORANGE);
		slider.setBounds(0, 20, 10, 200);
		slider.setForeground(new Color(240, 240, 240));
		slider.setOrientation(SwingConstants.VERTICAL);
		getContentPane().add(slider);
		
		JButton btnNewButton = new JButton("Cancelar\r\n");
		btnNewButton.setBackground(Color.RED);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.setBounds(127, 211, 85, 20);
		getContentPane().add(btnNewButton);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(20, 42, 222, 20);
		getContentPane().add(textPane);
		
		JCheckBox chckbxAceptasLosTerminos = new JCheckBox("Aceptas los terminos y condiciones. \r\n\r\n");
		chckbxAceptasLosTerminos.setBackground(Color.ORANGE);
		chckbxAceptasLosTerminos.setBounds(24, 181, 199, 23);
		getContentPane().add(chckbxAceptasLosTerminos);
		
		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBackground(Color.ORANGE);
		scrollBar.setBounds(417, 32, 17, 209);
		getContentPane().add(scrollBar);
		
		JLabel lblNombre = new JLabel("Nombre :\r\n");
		lblNombre.setBounds(20, 20, 75, 14);
		getContentPane().add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido :");
		lblApellido.setBounds(23, 73, 72, 14);
		getContentPane().add(lblApellido);
		
		JTextPane textPane_1 = new JTextPane();
		textPane_1.setBounds(20, 98, 222, 20);
		getContentPane().add(textPane_1);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Hombre\r\n");
		rdbtnNewRadioButton.setBackground(Color.ORANGE);
		rdbtnNewRadioButton.setBounds(68, 130, 63, 23);
		getContentPane().add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Mujer\r\n");
		rdbtnNewRadioButton_1.setBackground(Color.ORANGE);
		rdbtnNewRadioButton_1.setBounds(133, 130, 65, 23);
		getContentPane().add(rdbtnNewRadioButton_1);
		
		JLabel lblSexo = new JLabel("Sexo :");
		lblSexo.setBounds(23, 134, 46, 14);
		getContentPane().add(lblSexo);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(0, -3, 28, 20);
		getContentPane().add(comboBox_1);
		
		JLabel label = new JLabel("");
		label.setBackground(Color.WHITE);
		label.setIcon(new ImageIcon(ManuelAllozaMarco.class.getResource("/gui/icono.jpg")));
		label.setBounds(288, 105, 125, 119);
		getContentPane().add(label);
	}
}
